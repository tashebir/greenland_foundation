<%--
    Document   : Photo
    Created on : May 6, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>



<%@page language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*, java.io.*,java.sql.*,javax.sql.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>GreenLand Development Foundation </title>
            <script  language="javascript" type="text/javascript" src="/scripts/javascripts.js"></script>
             <link rel="shortcut icon" href="images/GDFH.gif" type="image/x-icon">
            <link href="css/style.css" rel="stylesheet" type="text/css"/>

        </head>
        <body>
            <div class="wrapper">
                <%@include file="/ssi/Gheader.jsp"%>
                <%@include file="/ssi/Gleft.jsp"%>
                <div class="phomidlenav">
                    <table border="0" cellspacing="0">
                        <tr> <td><h3 style="font-size:18px;text-align:center;"><strong>&nbsp;<u>Photo Gallery</u></strong></h3></td></tr>
                        <tr>
                            <td>
                                <table  border="0" cellspacing="0" cellpadding="5" style="line-height: 100%;" >
                                    <!--DWLayoutTablec1f38b-->

                                    <tr style="background-color:#d1ecbc ">

                                        <td width="750"  valign="top">
                                            <% int j=0;
                                            try {
 stmt = connection.createStatement();
              int rowCount = 0;
            int min=0;
            int max=0;

             rs = stmt.executeQuery("select * from photos order by id desc");
int numPages;
            
             min = rs.last() ? Integer.parseInt(rs.getString("id").toString()):0;
            rowCount = rs.last() ? rs.getRow() : 0; // Determine number of rows
max= rs.first()? Integer.parseInt(rs.getString("id").toString()):0;
            if (rowCount > 0) {
                // rs.beforeFirst();                     // We want next() to go to first row
                int pg = 1;
                Integer startp = 1;
                if (request.getParameter("pg") != null) {
                    pg = Integer.parseInt(request.getParameter("pg").toString());
                }
                numPages = (rowCount + 14) / 15;   //total number of pages where each page contains 15 rows
                Integer endp = numPages;
                int l1 = (pg - 1) * 15;
                int l2 = (rowCount - l1 < 15) ? rowCount - l1 : l1 + 15;
                rs = stmt.executeQuery("select * from photos order by id desc limit " + l1 + "," + 15 + " ");
                if ((pg - 1) <= 3) {
                    startp = 1;
                    endp = (numPages < 15) ? numPages : 15;
                } else if ((pg - 1) > 3) {
                    if ((numPages - pg) >= 2) {
                        startp = pg - 3;
                        endp = pg + 2;
                    } else if ((numPages - pg) < 2) {
                        int temp = numPages - 14;
                        startp = (temp > 0) ? temp : 1;
                    }
                }

session.setAttribute("min",min);
session.setAttribute("myink",max);
                                   %>
                                            <% 
                                                while (rs.next()) {
                                                    String Pname = rs.getString("photo");
                                                    String Description = rs.getString("description");
                                                    String id=rs.getString("id");
                                                    String tit=rs.getString("title");
                                      




                                            %>

                                            <a href="slides/before1.jsp?id=<%=id%>" title="<%=tit%>"><img src="Gallery/<%=Pname%>" border="6" style="border-color:#F7F3F0" vspace="2" width="130" height="97"></a>

                                            <% }  
 %>
                                        </td>
                                    </tr>
                                    <tr><td ><div style="height:18px;width:720px; background-color:#5a8e22;padding:5px;"><span style="float:left;color:#ffffff;">Page <%=pg%> of <%=numPages%></span> <span style="float:right;color:#ffffff;">
                                                    <%
          if (pg > 1) {
              out.println("<a class='anchorlink pg' href='?pg=1'>");
              out.println("First</a>&nbsp;&nbsp;");
              out.println("<a class='anchorlink pg' href='?pg=" + (pg - 1) + "'>");
              out.println("Previous</a>");
          } else {
              out.println("First&nbsp;&nbsp;Previous");
          }
          while (startp <= endp) {
              if (startp == pg) {
                  out.println("[" + startp + "]");
              } else {
                  out.println("&nbsp;&nbsp;" + "<a class='ganchor' href='?pg=" + startp + "'>" + startp + "</a>");
              }

              startp++;
          }
          if (pg != numPages) {
              out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + (pg + 1) + "'>" + "Next</a>");
              out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + numPages + "'>" + "Last</a>");
          } else {
              out.println("&nbsp;&nbsp;Next&nbsp;&nbsp;Last");
          }
      }
                                          %>


                                        </span></div>   </td>
                                    </tr>


                                </table>
                            </td>
                        </tr>
               </table>

                </div>
                <%

     rs.close();
      stmt.close();
      connection.close();
 } catch (Exception ex) {
                %>


                <%
         out.println("Unable to connect to database.");
      }
                %>


                <%@include file="/ssi/Gfooter.jsp"%>

            </div>
        </body>
    </html>

