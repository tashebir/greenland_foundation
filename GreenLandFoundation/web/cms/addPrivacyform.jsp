<%--
    Document   : addPrivacyform
 Created on    : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%><%
if(session.getAttribute("start")!=null && session.getAttribute("start").toString().equalsIgnoreCase("start")){
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Privacy Policy Management</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="../images/GDFH.gif" type="image" />
        <script type="text/javascript" src="js/view.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <link rel="stylesheet" type="text/css" href="css/view.css" media="all">
        <script type="text/javascript" language="javascript">
            function confirmadd()
            {return confirm(' Are you sure you want to add more than one Privacy policy text file?');}
        </script>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="Gheader.jsp"%>
            <%@include file="leftlinks.jsp"%>
            <div class="disp">
                <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
                %>

                <div id="form_container">


                    <form id="form1" class="appnitro" method="post" action="insertPrivacy.jsp" enctype="multipart/form-data" name="form1" OnSubmit='return confirmadd();' >
                        <div class="form_description">
                            <h2>Privacy Policy Posting Form</h2>
                            <p></p>
                        </div><ul>
                            <li id="li_17" >
                                <label class="description" for="element_17">Policy: </label>
                                <div>
                                    <textarea id="element_17" name="Policy" class="element textarea small"></textarea>
                                </div>
                            </li>		<li id="li_18" >
                                <label class="description" for="element_18">TitleofOverview: </label>
                                <div>
                                    <input id="element_18" name="titleofOverview" class="element text large" type="text" maxlength="255" value=""/>
                                </div>
                            </li>
                            <li id="li_19" >
                                <label class="description" for="element_19">Overview: </label>
                                <div>
                                    <textarea id="element_19" name="Overview" class="element textarea small"></textarea>
                                </div>
                            </li>		<li id="li_20" >
                                <label class="description" for="element_20">Titleofdetails: </label>
                                <div>
                                    <input id="element_20" name="titleofdetails" class="element text large" type="text" maxlength="255" value=""/>
                                </div>
                            </li>
                            <li id="li_22" >
                                <label class="description" for="element_22">Details: </label>
                                <div>
                                    <textarea id="element_21" name="Details" class="element textarea medium"></textarea>
                                </div>
                            </li>
                            <li class="buttons">
                                <input type="hidden" name="mynews" value="tsehay" />
                                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />

                            </li>
                        </ul>
                    </form>
                    <p>&nbsp;</p><p>&nbsp;</p>
                </div>
            </div>
            <%@include file="Gfooter.jsp"%>
        </div>
    </body>
</html>
<%}
else{
    out.println("Direct Access Denied!");
        %>
<%@include file="login.jsp"%>
<%
}
%>


