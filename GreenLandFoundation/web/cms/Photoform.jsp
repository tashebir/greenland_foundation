<%--
    Document : Photoform
  Created on :  May 31, 2010, 4:16:27 AM
    Author   : Tsehay Abera
    Email    : hellen_abera@yahoo.com
--%>
 <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
                %>

  <div id="form_container">


            <form id="form1" class="appnitro" method="post" action="insertPhoto.jsp" enctype="multipart/form-data" name="form1" >
                <div class="form_description">
                    <h2>Photo Uploading Form</h2>
                    <p></p>
                </div>
                <ul >
                  <li id="li_11" >
                        <label class="description" for="element_11">Title: </label>
                        <div>
                            <input id="element_11" name="title" class="element text large" type="text" maxlength="255" value=""/>
                        </div>
                    </li>
                    <li id="li_12" >
                        <label class="description" for="element_12">Photo: </label>
                        <div>
                            <input id="element_12" name="photo" class="element file" type="file" value="Select"/>
                        </div>
                    </li>
                    <li id="li_13" >
                        <label class="description" for="element_13">Description: </label>
                        <div>
                            <textarea id="element_13" name="description" class="element textarea small"></textarea>
                        </div>
                    </li>


                    <li>
                    <li class="buttons">
                        <input type="hidden" name="mynews" value="tsehay" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                    </li>
                </ul>
            </form>
            <p>&nbsp;</p><p>&nbsp;</p>
        </div>

