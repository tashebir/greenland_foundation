package org.apache.jsp.ssi;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class MissionHistory_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n");
      out.write("\"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>GreenLDF</title>\n");
      out.write("        <script type=\"text/javascript\" src=\"../scripts/read.js\"></script>\n");
      out.write("        <link href=\"../css/view.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body onload=\"ppppp()\">\n");
      out.write("\n");
      out.write("       <h3 style=\" font-size:14px;line-height:25px;\"><p>Greenland Development Foundation is a non-political,non-religious and no-profit making local Non Governmental\n");
      out.write("            Organization (NGO). The foundation was established by\n");
      out.write("            highly motivated individuals who are highly concerned\n");
      out.write("            about the sever land degradation and the alarming poverty\n");
      out.write("            situation of people in rural areas. The foundation was\n");
      out.write("            established by such commited individuals to contribute\n");
      out.write("            towards poverty eradication effort of various stakeholders.\n");
      out.write("            to realise its vision, goals and objectives, the founders\n");
      out.write("            have established close contact with the comminity members,\n");
      out.write("       governmental and non-governmental organizations.</p></h3>\n");
      out.write("\n");
      out.write("        <div id=\"nav\">\n");
      out.write("            <ul>\n");
      out.write("                <li><span onclick=\"javascript:vvvvvv(n='01')\" style=\"cursor:pointer\"><h3 style=\"font-size:14px;\"><p><b>Read More..</b></p></h3></span>\n");
      out.write("                    <ul id=\"read1\" style=\"display:none\">\n");
      out.write("                           <h3 style=\" font-size:14px;line-height:25px;\"><p>The mission of the foundation is to contributed towards\n");
      out.write("                            eradication rural poverty and bring sustainable socio-economic\n");
      out.write("                            development and improve the livelihood of the Greenland\n");
      out.write("                           Development Foundation.</p></h3>\n");
      out.write("                        <li><div ><span style=\"cursor:pointer\"  style=\"color:#800080\"onclick=\"javascript:location.href='/GreenLandFoundation/Mission.jsp'\"><h3 style=\"font-size:14px;\"><p><b>Brief Description</b></p></h3></span></div></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("            </ul>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
