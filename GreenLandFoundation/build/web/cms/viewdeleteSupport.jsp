<%--
    Document   : viewdeleteSupport
    Created on : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%><%
if(session.getAttribute("start")!=null && session.getAttribute("start").toString().equalsIgnoreCase("start")){
%>
<%

        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);


%>

<%@include file="/ssi/connection.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
        <title>View/Delete Support us</title>  
        <link rel="shortcut icon" href="../images/GDFH.gif" type="image" />
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript" src="js/view.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <link rel="stylesheet" type="text/css" href="css/view.css" media="all">
 <script type="text/javascript" language="javascript">
function confirmDelete()
{return confirm('Are you sure you want to delete this?');}
</script>

    </head>
    <body>
        <div class="wrapper">
            <%@include file="Gheader.jsp"%>
            <%@include file="leftlinks.jsp"%>
            <div class="disp">

                <%
        if (request.getParameter("inputform") != null || request.getParameter("clickform") != null) {
            String idhid = request.getParameter("idhid");
            String idinput = request.getParameter("idinput");
            Statement stat = connection.createStatement();
            if (idhid != null) {
                idinput = idhid;
            }

            ResultSet rs = stat.executeQuery("select * from Support where No='" + idinput + "'");
            int rowCount = rs.last() ? rs.getRow() : 0;
            if (rowCount == 0) //one1
            {
                out.println("<h3 style='text-align:center;color:red; font-weight:bold; text-decoration:blink; background:white;'>THE selected ID IS NOT FOUND</h3>");
            //include('delete1.php');
            } else //two1
            {
                
                int result = stat.executeUpdate("delete from Support where No='" + idinput + "'");
                if (result == 0) {
                    out.println("<h3 style='text-align:center; color:red; font-weight:bold; text-decoration:blink; background:white;'>THE Support IS NOT REMOVED</h3>");
                // include('delete1.php');
                } else {
                    out.println("<h3 style='text-align:center;color:#8cc63f; font-weight:bold; text-decoration:blink; background:white;'>THE Support HAS BEEN REMOVED</h3>");
                //include('delete1.php');
                }
            //} //kk

            }
        }
                %>




                <%
        Statement statf = connection.createStatement();
        ResultSet resultf = statf.executeQuery("select * from Support order by No desc");
                %>
                <div class="form_description">
                    <h2> Editing/Deleting Support Page </h2>
                    <p></p>
                </div><table border="1" align="center" width="98%" style="border-collapse:collapse;">
                    <tr bgcolor="#8cc63e" style="color:#ffffff;">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Adress</th>
                        <th>Email</th>
                        <th>Comments</th>
                        <th>DonationAmount</th>
                        <th>DonationType</th>
                        <th>Action (Delete)</th>
                    </tr>
                    <tr>
                        <%
        while (resultf.next()) {
                        %>
                        <td><%= resultf.getString("No")%></td>
                        <td><%= resultf.getString("Name")%></td>
                        <td><%= resultf.getString("Adress")%></td>
                        <td><%= resultf.getString("Email")%></td>
                        <td><%= resultf.getString("Comments")%></td>
                        <td><%= resultf.getString("DonationAmount")%></td>
                        <td><%= resultf.getString("DonationType")%></td>
                        <td>&nbsp;<form  action="" method='post'OnSubmit='return confirmDelete();'>
                                <input  type='hidden' name='idhid' value='<%= resultf.getString("No")%>'>
                                <input type='submit'  name="clickform" value='DELETE' >
                        </form></td>
                    </tr>
                    <%
        }
                    %>
                    <tr>

                        <td colspan="6" align="center">
                            <form   style="margin-top:0;" action="" method="post" OnSubmit='return confirmDelete();'>
                                INSERT THE ID <input type="text" name="idinput">	<input type="submit" name="inputform" value="DELETE"  >
                        </form>   </td>

                    </tr>

                </table>
            </div>
            <%@include file="Gfooter.jsp"%>
        </div>
    </body>
</html>
<%}
else{
    out.println("Direct Access Denied!");
        %>
<%@include file="login.jsp"%>
<%
}
%>
