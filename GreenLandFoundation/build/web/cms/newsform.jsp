<%--
    Document   : newsform
    Created on : May 31, 2010, 4:16:27 AM
    Author     : Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>
                <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
                %>

  <div id="form_container">


            <form id="form1" class="appnitro" method="post" action="insertnews.jsp" enctype="multipart/form-data" name="form1" >
                <div class="form_description">
                    <h2>News Posting Form</h2>
                    <p></p>
                </div>
                <ul >


                    <li id="li_8" >
                        <label class="description" for="element_8">Location: </label>
                        <div>
                            <select class="element select medium" id="element_8" name="location">
                                <option>Top News</option>
                                <option>Old News</option>

                            </select>
                        </div>
                    </li>

                    <li id="li_1" >
                        <label class="description" for="element_1">Title: </label>
                        <div>
                            <input id="element_1" name="title" class="element text large" type="text" maxlength="255" value=""/>
                        </div>
                    </li>		<li id="li_2" >
                        <label class="description" for="element_2">Description: </label>
                        <div>
                            <textarea id="element_2" name="description" class="element textarea small"></textarea>
                        </div>
                    </li>		<li id="li_3" >
                        <label class="description" for="element_3">Content: </label>
                        <div>
                            <textarea id="element_3" name="content" class="element textarea medium"></textarea>
                        </div>
                    </li>		<li id="li_4" >
                        <label class="description" for="element_4">Photo: </label>
                        <div>
                            <input id="element_4" name="photo" class="element file" type="file" value="Select"/>
                        </div>
                    </li>

                    <li id="li_5" >
                        <label class="description" for="element_5">Date: </label>
                        <span>
                            <input id="element_5_1" name="m" class="element text" size="2" maxlength="2" value="" type="text"> /
                            <label for="element_5_1">MM</label>
                        </span>
                        <span>
                            <input id="element_5_2" name="d" class="element text" size="2" maxlength="2" value="" type="text"> /
                            <label for="element_5_2">DD</label>
                        </span>
                        <span>
                            <input id="element_5_3" name="y" class="element text" size="4" maxlength="4" value="" type="text">
                            <label for="element_5_3">YYYY</label>
                        </span>

                        <span id="calendar_5">
                            <img id="cal_img_5" class="datepicker" src="images/calendar.gif" alt="Pick a date.">
                        </span>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField	 : "element_5_3",
                                baseField    : "element_5",
                                displayArea  : "calendar_5",
                                button		 : "cal_img_5",
                                ifFormat	 : "%B %e, %Y",
                                onSelect	 : selectDate
                            });
                        </script>

                    </li>
                    <li>
                    <li class="buttons">
                        <input type="hidden" name="mynews" value="tsehay" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                        <input id="reset" class="button_text" type="reset" name="reset" value="Reset" />
                    </li>
                </ul>
            </form>
            <p>&nbsp;</p><p>&nbsp;</p>
        </div>

