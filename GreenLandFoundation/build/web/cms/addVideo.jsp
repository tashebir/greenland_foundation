<%--
    Document   : addVideo
   Created on : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>
<%
if(session.getAttribute("start")!=null && session.getAttribute("start").toString().equalsIgnoreCase("start")){
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Video Management</title>
        <link rel="shortcut icon" href="../images/GDFH.gif" type="image" />
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="js/view.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <link rel="stylesheet" type="text/css" href="css/view.css" media="all">

    </head>
    <body>
        <div class="wrapper">
            <%@include file="Gheader.jsp"%>
            <%@include file="leftlinks.jsp"%>
            <div class="disp">
                <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
                %>

                <div id="form_container">


                    <form id="form1" class="appnitro" method="post" action="insertVideo.jsp" enctype="multipart/form-data" name="form1" >
                        <div class="form_description">
                            <h2>Video Posting Form</h2>
                            <p></p>
                        </div>
                        <ul >
	                         <li id="li_4" >
                                <label class="description" for="element_4">Video: </label>
                                <div>
                                    <input id="element_4" name="Video" class="element file" type="file" value="Select"/>
                                </div>
                            </li>


                            <li id="li_1" >
                                <label class="description" for="element_1">Title: </label>
                                <div>
                                    <input id="element_1" name="title" class="element text large" type="text" maxlength="255" value=""/>
                                </div>
                            </li>		<li id="li_2" >
                                <label class="description" for="element_2">Description: </label>
                                <div>
                                    <textarea id="element_2" name="description" class="element textarea small"></textarea>
                                </div>
                            </li>		

                          
                            <li class="buttons">
                                <input type="hidden" name="mynews" value="tsehay" />
                                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                            </li>
                        </ul>
                    </form>
                    <p>&nbsp;</p><p>&nbsp;</p>
                </div>
            </div>
            <%@include file="Gfooter.jsp"%>
        </div>
    </body>
</html>
<%}
else{
    out.println("Direct Access Denied!");
        %>
<%@include file="login.jsp"%>
<%
}
%>

