<%--
    Document   : leftlinks
    Created on : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body>
        <div class="leftdiv">

            <table class="leftdivtable" cellspacing="0"  >
                <tr>
                    <td class="header">News Management  </td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addnewsform.jsp">Add News</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditnews.jsp" >Delete/Edit News</a>

                    </td>
                </tr>

                <tr>
                    <td class="header tp">Photo Management</td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addPhotoform.jsp">Add Photo</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditphoto.jsp" >Delete/Edit Photo</a>

                    </td>
                </tr>
                <tr>
                    <td class="header tp">MissionHist Management</td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addmissionhistform.jsp">Add Mission</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditMissionHistory.jsp" >Delete/Edit Mission</a>

                    </td>
                </tr>
                <tr>
                    <td class="header tp">DeforestrationPoverty Management</td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addforest.jsp">Add Deforestration</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditDeforest.jsp" >Delete/Edit Deforestration</a>

                    </td>
                </tr>
                 <tr>
                    <td class="header tp">Leadership Management</td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addLeadership.jsp">Add Leadership</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditLeadership.jsp" >Delete/Edit Leadership</a>

                    </td>
                </tr>
                 <tr>
                    <td class="header">Video Management  </td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addVideo.jsp">Add Video</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deletedtVideo.jsp" >Delete/Edit Video</a>

                    </td>
                </tr>

                <tr>
                    <td class="header tp">Feedback of Blog Management</td>
                </tr>

                <tr>
                    <td class="leftrow">
                    <a class="subtitle" href="viewdeletBlog.jsp">View/Delete Feedback of Blog</a>
                </tr>
                 <tr>
                    <td class="header tp">JoinedourMailing List Management</td>
                </tr>

                <tr>
                    <td class="leftrow">
                    <a class="subtitle" href="viewsendemail.jsp">View/Delete List</a>
                </tr>
                <tr>
                    <td class="header tp">Supportpage Management</td>
                </tr>
                <tr>
                    <td class="leftrow">
                    <a class="subtitle" href="viewdeleteSupport.jsp">View/Delete </a>
                </tr>
                  <tr>
                    <td class="header tp">Link Management </td>
                </tr>
                                <tr>
                    <td class="leftrow"><a class="subtitle"  href="addlink.jsp">Add Link</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle"  href="deleteeditlink.jsp" >Delete/Edit Link</a>

                    </td>
                </tr>
                <tr>
                    <td class="header tp">Privacy Policy Management</td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addPrivacyform.jsp">Add Privacy</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditPrivacypolicy.jsp" >Delete/Edit Privacy</a>

                    </td>
                </tr>

                <tr>
                    <td class="header">Account Management  </td>
                </tr>
                <tr>
                    <td class="leftrow"><a class="subtitle" href="addAccount.jsp">Create Account</a>
                </tr>
                <tr>
                    <td class="leftrow">
                        <a class="subtitle" href="deleteeditAccount.jsp" >Remove/Change Account</a>

                    </td>
                </tr>
                
        </table>
            <div class="leftbottom "></div>
        </div>
    </body>
</html>
