<%--
    Document   : home
    Created on : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>
<%
 response.setHeader("Cache-Control", "no-cache");
 response.setHeader("Pragma", "no-cache");
 response.setDateHeader("max-age", 0);
 response.setDateHeader("Expires", 0);
%>
<% 
if(session.getAttribute("start")!=null && session.getAttribute("start").toString().equalsIgnoreCase("start")){
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Content Managment System Home Page</title>
                    <link href="css/style.css" rel="stylesheet" type="text/css"/>
 <link rel="shortcut icon" href="../images/GDFH.gif" type="image" />
    </head>
    <body>
        <div class="wrapper">
        <%@include file="Gheader.jsp"%>
        <%@include file="leftlinks.jsp"%>
        <div class="disp"><h1 style="font-weight:bolder;font-family:Nina, Raavi;color:#18305e;">&nbsp;
        
          Welcome To GreenLand Development Foundation<br>&nbsp;&nbsp;&nbsp;&nbsp;
          Content Managment System Home Page!
        </h1>
        <h2 style="font-weight:bolder;font-family:Nina, Raavi;color:#18305e;">Click On The Left To Manage Your Contents</h2>
</div>
 <%@include file="Gfooter.jsp"%>

</div>
    </body>
</html>
<%}
else{
    out.println("Direct Access Denied!");
        %>
<%@include file="login.jsp"%>
<%
}
%>