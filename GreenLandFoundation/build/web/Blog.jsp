<%--
    Document   : Blog
    Created on : May 6, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*, java.io.*,java.sql.*,javax.sql.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>GreenLand Development Foundation </title>
            <script  language="javascript" type="text/javascript" src="/scripts/javascripts.js"></script>
            <link rel="shortcut icon" href="images/GDFH.gif" type="image/x-icon">
            <link href="css/style.css" rel="stylesheet" type="text/css"/>

        </head>
        <body>
            <div class="wrapper">

                <%@include file="/ssi/Gheader.jsp"%>
                <%@include file="/ssi/Gleft.jsp"%>
                <div class="phomidlenav" >

                    <h3 style="font-size:18px;text-align:center;"><strong><u>Blog</u></strong></h3>

                    <%  try {
            stmt = connection.createStatement();
            int rowCount = 0;

            rs = stmt.executeQuery("select * from comments order by id desc");
            int numPages;

            rowCount = rs.last() ? rs.getRow() : 0; // Determine number of rows
            if (rowCount > 0) {
                // rs.beforeFirst();                     // We want next() to go to first row
                int pg = 1;
                int startp = 1;
                if (request.getParameter("pg") != null) {
                    pg = Integer.parseInt(request.getParameter("pg").toString());
                }
                numPages = (rowCount + 5) / 6;   //total number of pages where each page contains 15 rows
                int endp = numPages;
                int l1 = (pg - 1) * 6;
                int l2 = (rowCount - l1 < 6) ? rowCount - l1 : l1 + 6;
                rs = stmt.executeQuery("select * from comments order by id desc limit " + l1 + "," + 6 + " ");
                if ((pg - 1) <= 3) {
                    startp = 1;
                    endp = (numPages < 6) ? numPages : 6;
                } else if ((pg - 1) > 3) {
                    if ((numPages - pg) >= 2) {
                        startp = pg - 3;
                        endp = pg + 2;
                    } else if ((numPages - pg) < 2) {
                        int temp = numPages - 5;
                        startp = (temp > 0) ? temp : 1;
                    }
                }



                    %>


                    <!--<div id="divScroll-3" >-->
                            <%
                        while (rs.next()) {
                            String Name1 = rs.getString("Name");
                            String Comments1 = rs.getString("Comments");
                            String date = rs.getString("Date");


                    %>
                    <table>
                        <tr><td><h3 style=" font-size:14px;line-height:25px;"><p><%=Name1%> &nbsp;Says:&nbsp
                                <%= Comments1%></p></h3>
                        <p style="font-size:12px;">Posted Date <%= date%></p></td> </tr>

                    </table>

                    <%}%>
                    <h3 style="font-size:15px;text-align:right;">Your blog. Share your thoughts &nbsp;<a class="ganchor" href="welcomeJSF.jsp">Sign In</a> /
                    <a class="ganchor" href="BlogHome.jsp">Sign Up</a></h3>
                    <div style="height:18px;background-color:#5a8e22;padding:5px;"><span style="float:left;color:#ffffff;">Page <%=pg%> of <%=numPages%></span> <span style="float:right;color:#ffffff;">
                            <%
          if (pg > 1) {
              out.println("<a class='ganchorlink pg' href='?pg=1'>");
              out.println("First</a>&nbsp;&nbsp;");
              out.println("<a class='ganchorlink pg' href='?pg=" + (pg - 1) + "'>");
              out.println("Previous</a>");
          } else {
              out.println("First&nbsp;&nbsp;Previous");
          }
          while (startp <= endp) {
              if (startp == pg) {
                  out.println("[" + startp + "]");
              } else {
                  out.println("&nbsp;&nbsp;" + "<a class='ganchor' href='?pg=" + startp + "'>" + startp + "</a>");
              }

              startp++;
          }
          if (pg != numPages) {
              out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + (pg + 1) + "'>" + "Next</a>");
              out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + numPages + "'>" + "Last</a>");
          } else {
              out.println("&nbsp;&nbsp;Next&nbsp;&nbsp;Last");
          }
      }
                            %>

                    </span></div>


                    <!--</div>-->
                        <%
      // close all the connections.
      rs.close();
      stmt.close();
      connection.close();
  } catch (Exception ex) {
                    %>


                    <%
            out.println("Unable to connect to database.");
        }
                    %>






                </div>


                <%@include file="/ssi/Gfooter.jsp"%>

            </div>
        </body>
    </html>
</f:view>
