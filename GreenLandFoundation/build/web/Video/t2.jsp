
<%--
    Document   : Video
    Created on : May 31, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>GreenLand Development Foundation </title>
            <script  language="javascript" type="text/javascript" src="/scripts/javascripts.js"> </script>
            <link rel="shortcut icon" href="/images/GDFH.gif" type="image/x-icon">
 <script type="text/javascript" src="flowplayer-3.1.4.min.js"></script>

     <link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="style.css">
            <style type="text/css">

     .leftdivtable
     {

         width:155px;
         font-size:14px;
         background-color:#dde1dd;
     }
     .leftrow
     {
         /* border-bottom:1px solid;*/

         width:155px;

     }
     .leftdivtable a{

         height:28px;
         color:#18305e;
         width:155px;
         /* background-color:green;*/
         background-image:url(../images/lk1.png);
         background-repeat:no-repeat;
         display:block;
         font-weight:bold;
         border-bottom:1px solid #aaaaa9;
         padding-top:0.1em;
         padding-right:0.4em;
         padding-bottom:0.05em;
         padding-left:1.2em;
         margin-bottom:0px;
         text-decoration:none;
         /*text-transform:uppercase;*/
     }
     .leftdivtable a:hover
     {
         /* background-image:none;
         background-color:#fff301;*/
         background-image:url(../images/kl.png);
         background-repeat:no-repeat;
         color:#00704a;
         font-weight:bold;
         width:155px;
         text-decoration:none;
         border-bottom:1px solid #aaaaa9;
         height:28px;
         padding-top:0.1em;
         padding-right:0.4em;
         padding-bottom:0.05em;
         padding-left:1.2em;
     }
     .leftrow
     {

         width:155px;
     }

            </style>

        </head>
        <body>
            <div class="wrapper">
                <%@include file="ssi1/Gheader.jsp"%>
                <%@include file="ssi1/Gleft.jsp"%>
                <div class="megephoin">
                    <%  Class.forName("com.mysql.jdbc.Driver").newInstance();
        String connectionURL2 = "jdbc:mysql://localhost/greenlan_greenldf";
        Connection connection2 = DriverManager.getConnection(connectionURL2, "greenlan", "3qx5tar2595x");

Statement stmt1 = connection2.createStatement();
        int rowCount = 0;


        ResultSet rs1 = stmt1.executeQuery("select * from Video order by id desc");
        int numPages;

        rowCount = rs1.last() ? rs1.getRow() : 0; // Determine number of rows
        if (rowCount > 0) {
            // rs.beforeFirst();                     // We want next() to go to first row
            int pg = 1;
            int startp = 1;
            if (request.getParameter("pg") != null) {
                pg = Integer.parseInt(request.getParameter("pg").toString());
            }
            numPages = (rowCount + 5) / 6;   //total number of pages where each page contains 6 rows
            int endp = numPages;
            int l1 = (pg - 1) * 6;
            int l2 = (rowCount - l1 < 6) ? rowCount - l1 : l1 + 6;
            rs1 = stmt1.executeQuery("select * from Video order by id desc limit " + l1 + "," + 6 + " ");
            if ((pg - 1) <= 3) {
                startp = 1;
                endp = (numPages < 6) ? numPages : 6;
            } else if ((pg - 1) > 3) {
                if ((numPages - pg) >= 2) {
                    startp = pg - 3;
                    endp = pg + 2;
                } else if ((numPages - pg) < 2) {
                    int temp = numPages - 5;
                    startp = (temp > 0) ? temp : 1;
                }
            }
                    %>


                    <div class="rightop">Album List</div>
                    <%
                      rs1.beforeFirst();

                        while (rs1.next()) {
                            String id = rs1.getString("id");
                            String Name = rs1.getString("video");
                            String description = rs1.getString("description");
                            String title = rs1.getString("title");

                    %>

                    <table class="leftdivtable" cellspacing="-0.5px">

                        <tr><td class="leftrow"><a href="#" onclick="jj('<%=Name%>')"><%=title%>-
                                    <%  if (!description.equalsIgnoreCase("")) {%>
                            <%=description%><%}%></a></td>
                    <%}%>
					</tr> </table>


                    <div class="bot"><span style="float:left;">Page <%=pg%> of <%=numPages%></span> <span style="float:right;">
                            <%
            if (pg > 1) {
                out.println("<a class='ganchorlink pg' href='?pg=1'>");
                out.println("First</a>&nbsp;&nbsp;");
                out.println("<a class='ganchorlink pg' href='?pg=" + (pg - 1) + "'>");
                out.println("Previous</a>");
            } else {
                out.println("First&nbsp;&nbsp;Previous");
            }
            while (startp <= endp) {
                if (startp == pg) {
                    out.println("[" + startp + "]");
                } else {
                    out.println("&nbsp;&nbsp;" + "<a class='ganchor' href='?pg=" + startp + "'>" + startp + "</a>");
                }

                startp++;
            }
            if (pg != numPages) {
                out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + (pg + 1) + "'>" + "Next</a>");
                out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + numPages + "'>" + "Last</a>");
            } else {
                out.println("&nbsp;&nbsp;Next&nbsp;&nbsp;Last");
            }
        }
                            %>

                    </span></div>

                </div>


                <div class="middlevi">
                    <h3 style="font-size:18px;text-align:center;"><strong>&nbsp;<u>Video Display</u></strong></h3>
                    <div class="page" style="width:587px;height:440px;">





                        <a

                            style="display:block;width:587px;height:440px;"
                            id="player">
                        </a>
                        <script>
                            function jj(kk){
                                document.getElementById('player').href=kk;

                                flowplayer("player", "flowplayer-3.1.5.swf",{clip: {autoPlay: true},plugins: {controls:{all: true,stop:true,mute:true,time: true,play: true,scrubber: true,fullscreen:false,volume:true,tooltips: {buttons: true}}},play:{label: 'GDF',fadeSpeed: 3000}});
                            }
                        </script>
                    </div>


                </div>

                <%@include file="ssi1/Gfooter.jsp"%>

            </div>
        </body>
    </html>

