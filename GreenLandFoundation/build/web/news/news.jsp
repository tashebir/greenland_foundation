<%--
    Document   : news
    Created on : May 6, 2010, 4:16:27 AM
    Author     :Tsehay Abera
    Email      : hellen_abera@yahoo.com
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>GreenLand Development Foundation </title>
            <script  language="javascript" type="text/javascript" src="/scripts/javascripts.js"></script>
            <link rel="shortcut icon" href="/images/GDFH.gif" type="image/x-icon">
            <link href="/css/style.css" rel="stylesheet" type="text/css"/>
            <script type="text/javascript" src="/scripts/new.js"></script>
            <link href="/css/now.css" rel="stylesheet" type="text/css"/>
        </head>
        <body>
            <div class="wrapper">
                <%@include file="/ssi/Gheader.jsp"%>

                <%@include file="/ssi/Gleft.jsp"%>
                <div class="phomidlenav">
                    <h3 style="font-size:18px;text-align:center;"><strong><u>News</u></strong></h3>



                    <%
        stmt = connection.createStatement();
        int rowCount = 0;

        rs = stmt.executeQuery("select * from news order by id desc");
        int numPages;


        rowCount = rs.last() ? rs.getRow() : 0; // Determine number of rows
        if (rowCount > 0) {
            // rs.beforeFirst();                     // We want next() to go to first row
            int pg = 1;
            int startp = 1;
            if (request.getParameter("pg") != null) {
                pg = Integer.parseInt(request.getParameter("pg").toString());
            }
            numPages = (rowCount + 5) / 6;   //total number of pages where each page contains 6 rows
            int endp = numPages;
            int l1 = (pg - 1) * 6;
            int l2 = (rowCount - l1 < 6) ? rowCount - l1 : l1 + 6;
            rs = stmt.executeQuery("select * from news order by id desc limit " + l1 + "," + 6 + " ");
            if ((pg - 1) <= 3) {
                startp = 1;
                endp = (numPages < 6) ? numPages : 6;
            } else if ((pg - 1) > 3) {
                if ((numPages - pg) >= 2) {
                    startp = pg - 3;
                    endp = pg + 2;
                } else if ((numPages - pg) < 2) {
                    int temp = numPages - 5;
                    startp = (temp > 0) ? temp : 1;
                }
            }


            while (rs.next()) {
                String id = rs.getString("id");
                String title = rs.getString("title");
                String description = rs.getString("description");
                String photo = rs.getString("photo");
                String date = rs.getString("date");

                    %>
                    <div class="newslist">
                        <%  if (!photo.equalsIgnoreCase("")) {%>
                        <img src="images/<%=photo%>" alt="photo" width="100" height="100" class="imgs" /><%}%>
                        <h3 style=" font-size:14px;line-height:25px;"><b> <%=title%></b></h3><h3 style=" font-size:14px;line-height:25px;"><p><%=description%></p>
                        <a href="more.jsp?id=<%=id%>" class="ganchor" > <b> Read more...</b></a></h3>
                    </div>
                    <%}%>

                    <div style="height:18px;background-color:#5a8e22;padding:5px;">
                        <span style="float:left;color:#ffffff;">Page <%=pg%> of <%=numPages%></span>

                        <span style="float:right;color:#ffffff;">
                            <%
            if (pg > 1) {
                out.println("<a class='ganchorlink pg' href='?pg=1'>");
                out.println("First</a>&nbsp;&nbsp;");
                out.println("<a class='ganchorlink pg' href='?pg=" + (pg - 1) + "'>");
                out.println("Previous</a>");
            } else {
                out.println("First&nbsp;&nbsp;Previous");
            }
            while (startp <= endp) {
                if (startp == pg) {
                    out.println("[" + startp + "]");
                } else {
                    out.println("&nbsp;&nbsp;" + "<a class='ganchor' href='?pg=" + startp + "'>" + startp + "</a>");
                }

                startp++;
            }
            if (pg != numPages) {
                out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + (pg + 1) + "'>" + "Next</a>");
                out.println("&nbsp;&nbsp;<a class='ganchorlink pg' href='?pg=" + numPages + "'>" + "Last</a>");
            } else {
                out.println("&nbsp;&nbsp;Next&nbsp;&nbsp;Last");
            }
        }
                            %>

                        </span>
                    </div>


                </div>

                <%@include file="/ssi/Gfooter.jsp"%>

            </div>

        </body>
    </html>

